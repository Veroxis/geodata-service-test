#![allow(dead_code)]

use axum::{
    routing::post,
    http::StatusCode,
    response::IntoResponse,
    Json, Router,
};
use serde::{Serialize, Deserialize};
use std::net::SocketAddr;
use rayon::prelude::*;

#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[derive(Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
struct GeodataSet<'a> {
    pub country: Option<&'a str>,
    pub postal_code: &'a str,
    pub city: &'a str,
    pub state: Option<&'a str>,
    pub short_state: Option<&'a str>,
    pub county: Option<&'a str>,
    pub short_county: Option<&'a str>,
    pub community: Option<&'a str>,
    pub short_community: Option<&'a str>,
    pub latitude: Option<f64>,
    pub longitude: Option<f64>,
    pub accuracy: Option<f64>,
}

impl<'a> Eq for GeodataSet<'a> {}

impl<'a> Ord for GeodataSet<'a> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.city.cmp(other.city)
    }
}

lazy_static::lazy_static! {
    static ref RAW_CSV_DATA: &'static str = include_str!("../countries.csv");
    static ref GEO_DATA: Vec<GeodataSet<'static>> = RAW_CSV_DATA
        .split("\n")
        .skip(1)
        .filter_map(|row| {
            let row = row.trim();
            match row.is_empty() {
                true => None,
                false => {
                    let fields = row.split(",").map(|e|e.trim()).collect::<Vec<&str>>();
                    Some(GeodataSet {
                        country: match fields[0].is_empty() {
                            true => None,
                            false => Some(fields[0]),
                        },
                        postal_code: fields[1],
                        city: fields[2],
                        state: match fields[3].is_empty() {
                            true => None,
                            false => Some(fields[3]),
                        },
                        short_state: match fields[4].is_empty() {
                            true => None,
                            false => Some(fields[4]),
                        },
                        county: match fields[5].is_empty() {
                            true => None,
                            false => Some(fields[5]),
                        },
                        short_county: match fields[6].is_empty() {
                            true => None,
                            false => Some(fields[6]),
                        },
                        community: match fields[7].is_empty() {
                            true => None,
                            false => Some(fields[7]),
                        },
                        short_community: match fields[8].is_empty() {
                            true => None,
                            false => Some(fields[8]),
                        },
                        latitude: fields[9].parse().ok(),
                        longitude: fields[10].parse().ok(),
                        accuracy: fields[11].parse().ok(),
                    })
                },
            }
        })
        .collect::<Vec<GeodataSet<'static>>>();
}

#[derive(Debug, Serialize, Deserialize)]
struct SearchRequest {
    search: String
}

#[derive(Debug, Serialize, Deserialize)]
struct GeodataResponse<'a> {
    pub city: &'a str,
    pub country: Option<&'a str>,
    pub postal_code: &'a str,
    pub latitude: Option<f64>,
    pub longitude: Option<f64>,
}

impl<'a> From<&GeodataSet<'a>> for GeodataResponse<'a> {
    fn from(data: &GeodataSet<'a>) -> Self {
        GeodataResponse { city: data.city, country: data.country, postal_code: data.postal_code, latitude: data.latitude, longitude: data.longitude }
    }
}

fn find_cities(query: &str) -> Vec<&'static GeodataSet<'static>> {
    (*GEO_DATA).iter()
        .filter(|dataset| {
            dataset.city.to_lowercase().contains(query)
        })
        .collect()
}

fn find_zips(query: &str) -> Vec<&'static GeodataSet<'static>> {
    (*GEO_DATA).iter()
        .filter(|dataset| {
            dataset.postal_code.to_lowercase().contains(query)
        })
        .collect()
}

fn find_by_name_or_zip(query: &str) -> Vec<&'static GeodataSet<'static>> {
    let mut search_words = query.split(" ").collect::<Vec<&str>>();
    search_words.sort();
    search_words.dedup();
    let mut results = search_words.par_iter()
    .map(|word| {
        let (mut r1, mut r2) = rayon::join(|| find_cities(word), || find_zips(word));
        r1.append(&mut r2);
        r1
    })
    .flatten()
    .collect::<Vec<&'static GeodataSet<'static>>>();
    results.sort();
    results.dedup();
    results
}

async fn service_api_geodata(Json(payload): Json<SearchRequest>) -> impl IntoResponse {
    // tracing::info!("received query {:?}", payload);
    let geodata = find_by_name_or_zip(payload.search.to_lowercase().as_str());
    let result = geodata.iter().map(|element| GeodataResponse::from(*element)).collect::<Vec<GeodataResponse>>();
    // tracing::info!("finished result {:?}", payload);
    (StatusCode::OK, Json(result))
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing_subscriber::fmt::init();
    tracing::info!("Initializing...");
    lazy_static::initialize(&GEO_DATA);

    let app = Router::new()
        .route("/api/geodata", post(service_api_geodata));

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    tracing::info!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .expect("failed to serve webserver");

    Ok(())
}
